#!/bin/bash
sudo apt-get -y install apt-transport-https lsb-release ca-certificates
sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
sudo sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
sudo apt-get update
sudo apt-get -y install php7.2-common php7.2-cgi php7.2-cli php7.2-phpdbg php7.2-fpm \
libphp7.2-embed php7.2-dev php7.2-curl php7.2-gd php7.2-imap php7.2-interbase \
php7.2-intl php7.2-ldap php7.2-readline php7.2-odbc php7.2-pgsql php7.2-pspell \
php7.2-recode php7.2-tidy php7.2-xmlrpc php7.2 php7.2-json php-all-dev php7.2-sybase \
php7.2-sqlite3 php7.2-mysql php7.2-opcache php7.2-bz2 libapache2-mod-php7.2 php7.2-mbstring \
php7.2-pdo php7.2-dom php7.2-enchant php7.2-gmp php7.2-soap php7.2-zip