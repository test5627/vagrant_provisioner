echo '==== update ===='
sudo apt-get update
echo '==== changing the timezone ===='
sudo timedatectl set-timezone Europe/Moscow
echo '==== installing vim ===='
sudo apt-get install -y vim
echo '==== installing wget ===='
sudo apt-get install -y wget
echo '==== installing htop ===='
sudo apt-get install -y htop
echo '==== installing tmux ===='
sudo apt-get install -y tmux
echo '==== install php5.6 ===='
sudo apt-get -y install apt-transport-https lsb-release ca-certificates
sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
sudo sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
sudo apt-get update
sudo apt-cache search php5
sudo apt-get -y install php5.6-fpm --no-install-recommends
echo '==== installing Apache2 ===='
sudo apt-get install -y apache2
sudo sed -i 's/80/8888/' /etc/apache2/ports.conf
sudo sed -i 's/80/8888/' /etc/apache2/sites-enabled/000-default.conf
sudo sed -i 's/80/8888/' /etc/apache2/sites-available/000-default.conf
sudo service apache2 stop
echo '==== installing nginx ===='
sudo apt-get -y install nginx
sudo sed -i 's/\/var\/www\/html/\/var\/www\/default/' /etc/nginx/sites-available/default
sudo mkdir /var/www/default
sudo cp /var/www/html/index.nginx-debian.html /var/www/default/index.nginx-debian.html
sudo service nginx restart
sudo service apache2 start